var express = require('express');
var _ = require('underscore');
var router = express.Router();
var model = require('../model/model')
var datasource = require('../model/datasource')


router.get('/', function(req, res) {

    res.json(datasource.comments)
});




/* Filter comments on content property */
router.get('/filter', function(req, res) {

    var result = [];

    var queryContent =req.query.content;
    if (queryContent){
        datasource.comments.each(function(currentComment){

            var content = currentComment.get("content");

            //check if find some string
            if (content.toLowerCase().indexOf(queryContent.toLowerCase())>=0){
                //check if result contains
                var already = _.some(result, function(t){
                    return t.id == currentComment.id
                });

                if (!already){
                    result.push(currentComment)
                }
            }
        })
    }
    res.json(result)
});


/* GET comments listing. */
router.get('/:id', function(req, res) {

    var commentId = req.params.id;
    var comment = datasource.comments.find(function(c){
        return c.get("id")==commentId;
    });
    if (!comment){
        res.status(404);
        res.send({message:"no valid comment found with id :"+commentId });
        return;
    }

    res.json(comment)
});





router.put('/:id', function (req, res){
    console.dir(req.body)

    var comment = datasource.comments.find(function(c){
        return c.get("id")==req.params.id
    });
    comment.set("content", req.body.content);
    comment.set("tags", req.body.tags);

    res.json(comment)

});


router.post('/topic/:id', function (req, res){

    var data = req.body;

    var userId = null;
    if (! data.user || ! data.user.id){
        res.status(406);
        res.send({message:"no valid user provided"});
        return;
    }else{
        userId = data.user.id;
        data.user=datasource.users.findWhere({id:data.user.id});
        if (! data.user){
            res.status(406);
            res.send({message:"no valid user provided for id:"+userId});
            return;
        }
    }

    var topicId = req.params.id;
    var topic = datasource.topics.find(function(t){
        return t.get("id")== topicId
    });

    if (!topic){
        res.status(406);
        res.send({message:"no valid topic provided with id :"+ topicId});
        return;
    }

    var comment = new model.Comment(req.body)
    topic.attributes.comments.push(comment)

    datasource.comments.push(comment)
    res.json(comment)

});

module.exports = router;