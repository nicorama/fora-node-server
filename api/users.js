var express = require('express');
var router = express.Router();
var model = require('../model/model')
var datasource = require('../model/datasource')

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json()

/* GET users listing. */
router.get('/', function(req, res) {

  res.json(datasource.users);

});

router.get('/:id', function(req, res) {
    res.json(datasource.users.get(req.params.id));
});


router.get('/all', function(req, res) {
    res.json(datasource.users.sortBy("name"))
});


router.get('/admins', function(req, res) {


    res.json(datasource.users.filter(function(user){
        return user.get("admin")
    }))
});


router.get('/filter', function(req, res) {


    if (req.query.admin){
        req.query.admin = (req.query.admin =="true")
    }

    if (req.query.id){
        req.query.id = parseInt(req.query.id)
    }

    res.json(datasource.users.where(req.query))


});

router.put('/:id', function(req, res) {



    var user = req.body;
    console.log("req.params.id")
    var userInDatasource = datasource.users.find(function(user){
            return user.id == req.params.id;
    });

    userInDatasource.set("statement", user.statement);
    userInDatasource.set("admin", user.admin);
    console.log(userInDatasource.toJSON())

    ///--> KEEP
    res.json(userInDatasource);
});

router.post('/', jsonParser, function(req, res) {

    var data = req.body;
    console.log(data)
    var id = datasource.users.length +1;
    data.id = id;

    //setting admin false by default
    if (! (data.admin ===true)){
        data.admin =false;
    }

    //check email unicity
    var already = datasource.users.findWhere({email:data.email});
    if (already){
        res.status(409);
        res.send({message:"email "+data.email+" already exists"});
        return;
    }

    var newUser = new model.User(data);
    datasource.users.push(newUser);

    res.json(newUser.toJSON());
});



module.exports = router;
