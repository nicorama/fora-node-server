var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: 'Express' });
});

/* GET home page. */
router.get('/login', function(req, res) {

    res.cookie('csrf', 'enobkcab');
    res.render('index', { title: 'You have a csrf cookie' });
});

module.exports = router;
