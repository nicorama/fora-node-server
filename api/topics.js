var express = require('express');
var _ = require('underscore');
var router = express.Router();
var model = require('../model/model')
var datasource = require('../model/datasource')


/* GET topics listing. */
router.get('/', function(req, res) {

    res.json(datasource.topics)
});


/* GET topics listing. */
router.get('/filter', function(req, res) {

    var result = [];

    if (req.query.user){
        datasource.topics.each(function(topic){

            var name = topic.get("user").name;

            //check if result contains
            var already = _.some(result, function(t){
                return t.id == topic.id
            })
            if (already){
                return false;
            }else{
                if (name.toLowerCase().indexOf(req.query.user.toLowerCase())>=0){
                    result.push(topic)
                }
            }
        })

    }
    console.log(result)
    res.json(result)
});

/* GET topics listing. */
router.get('/:id', function(req, res) {

    var topic = datasource.topics.find(function(t){
        return t.get("id")==req.params.id
    })

    res.json(topic.toJSON())
});




router.post('/',  function(req, res) {

    var data = req.body;
    data.id =datasource.topics.length +1;


    var userId = null;
    if (! data.user || ! data.user.id){
        res.status(406);
        res.send({message:"no valid user provided"});
        return;
    }else{
        userId = data.user.id;
        data.user=datasource.users.findWhere({id:data.user.id});
        if (! data.user){
            res.status(406);
            res.send({message:"no valid user provided for id:"+userId});
            return;
        }
    }

    var newTopic = new model.Topic(data);
    datasource.topics.push(newTopic);

    res.json(newTopic.toJSON());
});


router.put('/:id/tags', function(req, res) {

    var topicId = req.params.id;
    var topicInDatasource = datasource.topics.find(function(t){
        return t.id == topicId
    });

    if (!topicInDatasource){
        res.status(404);
        res.send({message:"no valid topic with id : "+topicId});
        return;
    }

    var tags=req.body;
    if (!_.isArray(tags)){
        res.status(406);
        res.send({message:"request body must be a array of tags : "});
        return;
    }else{
        topicInDatasource.set("tags", tags);
    }

    res.json(topicInDatasource);
});

module.exports = router;